using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEditor;
using System.Linq;

public class MenuController : MonoBehaviour /*, IPointerDownHandler, IPointerUpHandler*/ {
    //public GameObject panel;
    public GameObject frame;
    public Texture empty;
    public Texture selected;
    public Texture locked;
    public Button playButton;
    public Image grid;

    private bool[] _selectedWarriors = new bool[10]; // TODO. Poner una constante = 10
    private GameObject _closeButton;

    private int _warriorsToPlay = 3; // TODO. Pasar a una constante
    private float _startTime;
    private int _targetId = -1;
    private GameObject _featuresPanel;

    // Use this for initialization
    void Start() {
        //int warriors = 10; // TODO. Basar esto en el array de warriors
        int enabledWarriors = 5; // 3

        GameObject[] warriorPrefabs = GameManager.Instance.warriorPrefabs;

        for (int i = 0; i < warriorPrefabs.Length; i++) {
            Vector3 pos = new Vector3(0, 0, 0);
            GameObject frame2 = Instantiate(frame, pos, Quaternion.identity, grid.transform);
            GameObject warrior = Instantiate(warriorPrefabs[i], pos, Quaternion.identity, this.transform) as GameObject;
        }
    }


    // https://stackoverflow.com/questions/50865533/how-can-i-dynamically-generate-a-gui-in-unity3d-via-scripting
    GameObject ImageViewBuilder(Vector2 size, Vector2 position, Transform objectToSetImageView, Texture texture, Color color) {
        GameObject imageView = new GameObject("ImageView", typeof(RectTransform));
        RawImage image = imageView.AddComponent<RawImage>();
        image.texture = texture;
        image.color = color;
        RectTransform rectTransform = imageView.GetComponent<RectTransform>();
        rectTransform.sizeDelta = size;
        rectTransform.anchoredPosition = position;

        // https://answers.unity.com/questions/1225118/solution-set-ui-recttransform-anchor-presets-from.html
        //rectTransform.pivot = new Vector2(1, 1);
        rectTransform.anchorMin = new Vector2(0.5f, 0);
        rectTransform.anchorMax = new Vector2(0.5f, 0);

        imageView.transform.SetParent(objectToSetImageView, false);

        return imageView;
    }

    void SetTexture(GameObject imageView, Texture texture) {
        RawImage image = imageView.GetComponent<RawImage>();
        image.texture = texture;
    }

    /*public void OnPointerClick(PointerEventData eventData) {
        
        Debug.Log("Click DETECTED ON RIFFLE IMAGE");

    }*/

    public void OnPlayButtonClick() {
    }

    public void OnCloseButtonClick() {
    }

    public void OnPointerDownOption(int id) {
    }

    public void OnPointerUpOption(GameObject imageView) {
    }
       
}
