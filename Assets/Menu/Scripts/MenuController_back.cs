using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEditor;
using System.Linq;

public class MenuController_back : MonoBehaviour /*, IPointerDownHandler, IPointerUpHandler*/ {
    //public GameObject panel;
    public Texture frame;
    public Texture empty;
    public Texture selected;
    public Texture locked;

    private bool[] _selectedWarriors = new bool[10]; // TODO. Poner una constante = 10
    private Button _playButton;
    private GameObject _closeButton;

    private int _warriorsToPlay = 3; // TODO. Pasar a una constante
    private float _startTime;
    private int _targetId = -1;
    private GameObject _featuresPanel;

    // Use this for initialization
    void Start() {
        // Play Button
        _playButton = this.transform.Find("Play Button").gameObject.GetComponent<Button>();
        _playButton.onClick.AddListener(OnPlayButtonClick);

        // Desactivo el panel features
        _featuresPanel = this.transform.Find("Features").gameObject;
        _featuresPanel.SetActive(false);

        // Close Button
        _closeButton = _featuresPanel.transform.Find("Close Button").gameObject;
        EventTrigger triggerButton = _closeButton.AddComponent<EventTrigger>();
        EventTrigger.Entry entryPointerClick = new EventTrigger.Entry();
        entryPointerClick.eventID = EventTriggerType.PointerClick;
        entryPointerClick.callback = new EventTrigger.TriggerEvent();
        entryPointerClick.callback.AddListener(delegate { OnCloseButtonClick(); });
        triggerButton.triggers.Add(entryPointerClick);


        int warriors = 10; // TODO. Basar esto en el array de warriors
        int enabledWarriors = 5; // 3

        GameObject[] warriorPrefabs = GameManager.Instance.warriorPrefabs;

        //Debug.Log("Screen.width:" + Screen.width + ", Screen.height:" + Screen.height);
        int rows = 2;
        int cols = (int)Math.Ceiling((double)(warriors / rows));
        //Debug.Log("Cols:" + cols);

        // Frame
        float frameX = -8;
        float frameY = 2.5f;
        float frameWidth = 2.6f; // 2.3f;
        float frameHeight = 3f;

        // Icons
        float iconWidth = 1f;
        float iconHeight = 1f;

        //float leftSideOfScreen = Camera.main.transform.position.x - Camera.main.orthographicSize * Screen.width / Screen.height;
        int wIdx = 0, wId = 0;
        for (int r = rows-1; r >= 0; r--) {
            for (int c = 0; c < cols; c++) {

                // Frame
                float offsetX = (c < cols) ? 0.4f : 0f;
                float offsetY = (r < rows) ? 0.4f : 0f;

                GameObject frameImage = ImageViewBuilder(new Vector2(frameWidth, frameHeight), new Vector2(frameX + frameWidth * c + offsetX * c, frameY + frameHeight * r + offsetY * r), this.transform, frame, Color.white);

                // Selection Mark
                Color color = (wId < enabledWarriors) ? Color.green : Color.red;
                Texture texture = (wId < enabledWarriors) ? empty : locked;
                GameObject iconMark = ImageViewBuilder(new Vector2(iconWidth, iconHeight), new Vector2(frameX + frameWidth * 0.45f + frameWidth * c + offsetX * c, frameY - frameHeight * 0.45f + frameHeight * r + offsetY * r), this.transform, texture, color);
                GameObject selectionMark = (wId < enabledWarriors) ? iconMark : null;

                EventTrigger triggerFrame = frameImage.AddComponent<EventTrigger>();
                EventTrigger triggerMark = iconMark.AddComponent<EventTrigger>();

                // On Pointer Down
                int id = wId; // Workarround to create another reference in memory to pass through the callback
                EventTrigger.Entry entryPointerDown = new EventTrigger.Entry();
                entryPointerDown.eventID = EventTriggerType.PointerDown;
                entryPointerDown.callback = new EventTrigger.TriggerEvent();
                entryPointerDown.callback.AddListener(delegate { OnPointerDownOption(id); });
                triggerFrame.triggers.Add(entryPointerDown);
                triggerMark.triggers.Add(entryPointerDown);

                // On Pointer Up
                EventTrigger.Entry entryPointerUp = new EventTrigger.Entry();
                entryPointerUp.eventID = EventTriggerType.PointerUp;
                entryPointerUp.callback = new EventTrigger.TriggerEvent();
                entryPointerUp.callback.AddListener(delegate { OnPointerUpOption(selectionMark); });                
                triggerFrame.triggers.Add(entryPointerUp);
                triggerMark.triggers.Add(entryPointerUp);                

                // Warrior
                Vector3 pos = new Vector3(frameX + 0.1f + frameWidth * c + offsetX * c, frameY - 1.15f + frameHeight * r + offsetY * r, 0);
                GameObject warrior = Instantiate(warriorPrefabs[wIdx], pos, Quaternion.identity, this.transform) as GameObject;
                

                // warrior.transform.SetAsLastSibling(); // .SetSiblingIndex(0);
                //warrior.transform.SetParent(this.transform, false); // https://answers.unity.com/questions/1226961/instantiate-prefab-from-script-inside-a-canvas.html

                //Debug.Log("Warrior layer:" + warrior.layer + ", sorting layer:" + warrior.GetComponent<SortingGroup>().sortingLayerName + " - Card layer:" + card.layer + ", sorting layer:" + card.GetComponent<SortingGroup>().sortingLayerName);

                //warrior.GetComponent<SortingGroup>().sortingLayerName = "Default";
                //warrior.GetComponent<SortingGroup>().sortingOrder = 0;

                // TODO. Activar
                //warrior.GetComponent<WarriorController>().enabled = false;
                //warrior.GetComponent<WarriorStateMachine>().enabled = false;

                //warrior.GetComponent<WarriorSortingOrder>().enabled = false;
                //warrior.GetComponent<SortingGroup>().enabled = false;
                warrior.GetComponent<Animator>().enabled = false; // TODO. Activar al seleccionar el warrior

                //warrior.GetComponent<SortingGroup>().sortingLayerName = "Background";
                //warrior.GetComponent<SortingGroup>().sortingOrder = 2;
                /*warrior.AddComponent<SpriteRenderer>();
                warrior.GetComponent<SpriteRenderer>().sortingLayerName = "Menu";
                warrior.GetComponent<SpriteRenderer>().sortingOrder = 2;*/

                //warrior.GetComponent<Animator>().StopPlayback();
                warrior.transform.localScale = new Vector3(0.3f, 0.3f, 1f);

                // Alternativa
                //Texture2D icon = AssetPreview.GetAssetPreview(warriorPrefabs[wIdx]);
                //ImageViewBuilder(new Vector2(frameWidth, frameHeight), new Vector2(frameX + frameWidth * c + offsetX * c, frameY + frameHeight * r + offsetY * r), this.transform, icon);

                wId++;
                wIdx++;
                if (wIdx == warriorPrefabs.Length) wIdx = 0;


            } // for

        } // for

    }


    // https://stackoverflow.com/questions/50865533/how-can-i-dynamically-generate-a-gui-in-unity3d-via-scripting
    GameObject ImageViewBuilder(Vector2 size, Vector2 position, Transform objectToSetImageView, Texture texture, Color color) {
        GameObject imageView = new GameObject("ImageView", typeof(RectTransform));
        RawImage image = imageView.AddComponent<RawImage>();
        image.texture = texture;
        image.color = color;
        RectTransform rectTransform = imageView.GetComponent<RectTransform>();
        rectTransform.sizeDelta = size;
        rectTransform.anchoredPosition = position;

        // https://answers.unity.com/questions/1225118/solution-set-ui-recttransform-anchor-presets-from.html
        //rectTransform.pivot = new Vector2(1, 1);
        rectTransform.anchorMin = new Vector2(0.5f, 0);
        rectTransform.anchorMax = new Vector2(0.5f, 0);

        imageView.transform.SetParent(objectToSetImageView, false);

        return imageView;
    }

    void SetTexture(GameObject imageView, Texture texture) {
        RawImage image = imageView.GetComponent<RawImage>();
        image.texture = texture;
    }

    /*public void OnPointerClick(PointerEventData eventData) {
        
        Debug.Log("Click DETECTED ON RIFFLE IMAGE");

    }*/

    public void OnPlayButtonClick() {
        if (!_featuresPanel.activeSelf) {

            Debug.Log("Click On Play Button");
            // TODO.

        }
    }

    public void OnCloseButtonClick() {
        Debug.Log("Click On Close Button");
        //_featuresPanel.SetActive(false);
    }

    public void OnPointerDownOption(int id) {
        if (!_featuresPanel.activeSelf) {
            _targetId = id;
            _startTime = Time.time;
            //Debug.Log("Pointer down warrior: " + _targetId);
        }
    }

    public void OnPointerUpOption(GameObject imageView) {
        if (!_featuresPanel.activeSelf) {
            float timeSinceStart = Time.time - _startTime;
            //Debug.Log("Pointer up. Time:" + timeSinceStart + ", Pointer down warrior: " + _targetId);
            if (timeSinceStart < 3f) {
                if (imageView != null) {

                    int selCount = _selectedWarriors.Count(w => w);
                    bool canMark = (selCount < _warriorsToPlay) || (selCount == _warriorsToPlay && _selectedWarriors[_targetId]);
                    if (canMark) {
                        _selectedWarriors[_targetId] = !_selectedWarriors[_targetId];
                        Texture texture = (_selectedWarriors[_targetId]) ? selected : empty;
                        SetTexture(imageView, texture);
                    } else {
                        /// TODO. Mensaje "Puedes elegir solo 3"
                    }

                    // Enable or disable Play Button based on the amount of warriors selected
                    _playButton.interactable = (_selectedWarriors.Count(w => w) == _warriorsToPlay);
                }
            } else {
                _featuresPanel.SetActive(true);
                Debug.Log("Info warrior: " + _targetId);
                // TODO, despliegue del PupUp
            }
        }
    }
       
}
