using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieStateMachine : StateMachine
{
    /*[HideInInspector]
    public IdleState idleState;

    [HideInInspector]
    public AttackState attackState;

    [HideInInspector]
    public FightState fightState;

    [HideInInspector]
    public StopState abandonState;*/

    [HideInInspector]
    public bool isReachEnemy;

    [HideInInspector]
    public Animator anim;

    [HideInInspector]
    public float speed = 0.2f;

    private void Awake()
    {
        //idleState = new IdleState(this);
        //attackState = new AttackState(this);

        //isPicked = false;
        isReachEnemy = false;
        anim = GetComponent<Animator>();
    }

    /*protected override BaseState GetInitialState()
    {
        return idleState;
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Do something
        isReachEnemy = true;
        //print("Zombie Collide");
    }
   

}
