using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Warrior Card")]
public class Card : ScriptableObject {

    public new string name;
    public Sprite image;

    public float health;
    public float power;
    public float experience;
    public float level;

}
