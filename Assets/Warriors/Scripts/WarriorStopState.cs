using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorStopState : BaseState
{
    private WarriorStateMachine _sm;
    private WarriorController _c;

    private float _t;
    private Vector2 _pointA; 
    private Vector2 _pointB;
    private GameObject _zombie;

    public WarriorStopState(WarriorStateMachine stateMachine, WarriorController controller) : base("Stop", stateMachine) {
        _sm = stateMachine;
        _c = controller;
    }

    public override void Enter()
    {
        base.Enter();
        _zombie = GameManager.Instance.zombie;
        _t = 0f;
        // Flip
        Vector3 flip = _c.transform.localScale;
        flip.x *= -1f;
        _c.transform.localScale = flip;
        _c.anim.Play("Rogue_run_01");

        _pointA = _c.transform.position; //_zombie.transform.position - new Vector3(1.6f, 2.6f);
        _pointB = new Vector3(-7.4f, 1.15f, 2f); // TODO. Colocar en el Objeto instanciado desde la clase Warrior!!!
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();
        if (_t >= 1.0f) { // Reemplazar esto por un objeto Empty llamaso Spawn Position !!!
            _sm.ChangeState(_sm.idleState);
        }
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();

        _t += Time.deltaTime * _c.speed;
        _c.transform.position = Vector2.Lerp(_pointA, _pointB, _t);

        if (_t >= 1.0f) { // Reemplazar esto por un objeto Empty llamaso Spawn Position !!!
            // Flip
            Vector3 flip = _c.transform.localScale;
            flip.x *= -1f;
            _c.transform.localScale = flip;
        }
    }

}
