using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorController : MonoBehaviour {
    // TODO. Intentar basar esta clase en la CharacterBattle.cs del video https://www.youtube.com/watch?v=0QU0yV0CYT4 (Simple Turn-Based RPG Battle System)

    [HideInInspector] public bool isPicked;
    [HideInInspector] public bool isReachEnemy;
    [HideInInspector] public Animator anim;
    [HideInInspector] public float speed;
    [HideInInspector] public GameObject zombie;

    private HealthSystem _healthSystem;
    protected WarriorStateMachine _stateMachine;

    private void Awake() {
        _stateMachine = gameObject.AddComponent<WarriorStateMachine>();
    }

    private void Start() {
        speed = 1.0f;
        isPicked = false;
        isReachEnemy = false;
        anim = GetComponent<Animator>();
        zombie = GameManager.Instance.zombie;
    }

    void OnMouseDown() {
        isPicked = true;
        //print("Warrior Picked");
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        isReachEnemy = true;
        //print("Warrior Collide");
    }




}

