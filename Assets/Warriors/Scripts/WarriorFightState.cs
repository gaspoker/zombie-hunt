using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorFightState : BaseState
{
    private WarriorStateMachine _sm;
    private WarriorController _c;

    private int _attackCount;
    private GameObject _zombie;

    public WarriorFightState(WarriorStateMachine stateMachine, WarriorController controller) : base("Fight", stateMachine) {
        _sm = stateMachine;
        _c = controller;
    }

    public override void Enter()
    {
        base.Enter();
        _zombie = GameManager.Instance.zombie;
        _attackCount = 0;
        _c.anim.Play("Rogue_attack_01");
        //_zombie.GetComponent<Animator>().Play("idle"); // anim debería estar dentro de la clase Zombie
        //_animPlay = true;
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();

        if (_attackCount == 3) { 
            _sm.ChangeState(_sm.stopState);
        }
    }

    public override void UpdatePhysics()
    {
        base.UpdatePhysics();
        // Chequeo el final de la animación para el conteno
        // https://gamedev.stackexchange.com/questions/117423/unity-detect-animations-end

        // Variar entre Rogue_attack_01 y Rogue_attack_03
        if(_c.anim.GetCurrentAnimatorStateInfo(0).IsName("Rogue_attack_01") && _c.anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f) {
            _c.anim.Play("Rogue_idle_01");
        }

        // Zombie anim
        if(_attackCount == 0 && _c.anim.GetCurrentAnimatorStateInfo(0).IsName("Rogue_attack_01") && _c.anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.3f) {
            _zombie.GetComponent<Animator>().Play("hurt"); // anim debería estar dentro de la clase ZombieController?
            //_zombie.setColorTint(new Color(1, 0, 0, 1f)); // Rojo
        }


        if(_c.anim.GetCurrentAnimatorStateInfo(0).IsName("Rogue_idle_01") && _c.anim.GetCurrentAnimatorStateInfo(0).normalizedTime == 0f) {
            _c.anim.Play("Rogue_attack_01");
            //_zombie.GetComponent<Animator>().Play("idle"); // anim debería estar dentro de la clase Zombie
            GameManager.Instance.healthSystem.Damage(10);
            _attackCount++;
            //Debug.Log("Attack count:" + _attackCount);
        }

        if (_attackCount == 3) {
            _zombie.GetComponent<Animator>().Play("idle"); // anim debería estar dentro de la clase Zombie
        }

        //Debug.Log("normalizedTime: " + _sm.anim.GetCurrentAnimatorStateInfo(0).normalizedTime);
        //Debug.Log("state: " + _sm.anim.GetCurrentAnimatorStateInfo(0));
    }

}
