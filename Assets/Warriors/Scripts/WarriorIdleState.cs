using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorIdleState : BaseState
{
    private WarriorStateMachine _sm;
    private WarriorController _c;

    public WarriorIdleState(WarriorStateMachine stateMachine, WarriorController controller) : base("Idle", stateMachine) {
        _sm = stateMachine;
        _c = controller;
    }

    public override void Enter()
    {
        base.Enter();
        _c.anim.Play("Rogue_idle_01");
        _c.isPicked = false;
    }

    public override void UpdateLogic()
    {
        base.UpdateLogic();
        if (_c.isPicked) {
            _sm.ChangeState(_sm.attackState);
        }
    }

}
