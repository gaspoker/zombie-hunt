using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorStateMachine : StateMachine {

   /* public enum WarriorEventType {
        IDLE, ATTACK, FIGHT, STOP
    }*/

    public WarriorIdleState idleState;
    public WarriorAttackState attackState;
    public WarriorFightState fightState;
    public WarriorStopState stopState;

    private void Awake() {
        WarriorController controller = GetComponent<WarriorController>();

        idleState = new WarriorIdleState(this, controller);
        attackState = new WarriorAttackState(this, controller);
        fightState = new WarriorFightState(this, controller);
        stopState = new WarriorStopState(this, controller);
    }

    protected override BaseState GetInitialState() {
        return idleState;
    }

}
