using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarriorAttackState : BaseState
{
    private WarriorStateMachine _sm;
    private WarriorController _c;

    private float _t;
    private Vector2 _pointA; 
    private Vector2 _pointB;

    public WarriorAttackState(WarriorStateMachine stateMachine, WarriorController controller) : base("Attack", stateMachine) {
        _sm = stateMachine;
        _c = controller;
    }

    public override void Enter() {
        base.Enter();

        _c.isReachEnemy = false;
        _t = 0f;
        _pointA = _c.transform.position;
        _pointB = _c.zombie.transform.position - new Vector3(1.6f, 2.6f);

        //Debug.Log("Point A:" + _pointA + " Point B:" + _pointB);

        //Debug.Log("speed:" + _c.speed);

        _c.anim.Play("Rogue_run_01");

    }

    public override void UpdateLogic() {
        base.UpdateLogic();

        if (_c.isReachEnemy) {
            _sm.ChangeState(_sm.fightState);
        }
    }

    public override void UpdatePhysics() {
        base.UpdatePhysics();

        //Debug.Log("Time.deltaTime:" + Time.deltaTime);

        _t += Time.deltaTime * _c.speed;
        //Debug.Log("speed:" + _c.speed);

        _c.transform.position = Vector2.Lerp(_pointA, _pointB, _t);
    }


}
