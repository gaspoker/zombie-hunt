using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.Rendering;


public enum GameState { Menu, PlayerTurn, EnemyTurn, Won, lost } // https://www.youtube.com/watch?v=_1pz_ohupPs

public class GameManager : MonoBehaviour
{

    // Mejorar segun https://www.youtube.com/watch?v=4I0vonyqMi8&t=357s (Game Manager - Controlling the flow of your game [Unity Tutorial])
    public static GameManager Instance { get; private set; }

    [SerializeField] public GameObject[] warriorPrefabs; // Normalizar nombres
    [SerializeField] public GameObject zombiePrefab; // Normalizar nombres
    [SerializeField] public Transform pfHealthBar;

    [HideInInspector] public GameObject zombie; // TODO. Crear clase Zombie con todos los atributos necesarios
    [HideInInspector] public HealthSystem healthSystem;

    private bool _isGameOver;
    private GameObject[] _warriors;

    private void Awake() {
        Instance = this;


        /*_warriors = new GameObject[3];

        // Spawn Warriors
        int wIdx1 = 0; // Random.Range(0, warriorPrefabs.Length - 1);
        int wIdx2 = 0; //Random.Range(0, warriorPrefabs.Length - 1);
        int wIdx3 = 0; //Random.Range(0, warriorPrefabs.Length - 1);

        // Warrior 1
        Vector3 wPos1 = new Vector3(-8f, 4.4f);
        GameObject w1 = Instantiate(warriorPrefabs[wIdx1], wPos1, Quaternion.identity);
        w1.transform.localScale = new Vector3(0.3f, 0.3f, 1f);
        // Health Bar
        healthSystem = new HealthSystem(100);
        Transform hb1 = Instantiate(pfHealthBar, wPos1 + new Vector3(0f, 3f), Quaternion.identity);
        hb1.GetComponent<HealthBar>().Setup(healthSystem);
        _warriors[0] = w1;

        // Warrior 2
        Vector3 wPos2 = new Vector3(-9f, 2.75f);
        GameObject w2 = Instantiate(warriorPrefabs[wIdx2], wPos2, Quaternion.identity);
        w2.transform.localScale = new Vector3(0.3f, 0.3f, 1f);
        Transform hb2 = Instantiate(pfHealthBar, wPos2 + new Vector3(0f, 3f, 0f), Quaternion.identity);
        hb2.GetComponent<HealthBar>().Setup(healthSystem);
        _warriors[1] = w2;

        // Warrior 3
        Vector3 wPos3 = new Vector3(-7.4f, 1.15f);
        GameObject w3 = Instantiate(warriorPrefabs[wIdx3], wPos3, Quaternion.identity);
        w3.transform.localScale = new Vector3(0.3f, 0.3f, 1f);
        Transform hb3 = Instantiate(pfHealthBar, wPos3 + new Vector3(0f, 3f, 0f), Quaternion.identity);
        hb3.GetComponent<HealthBar>().Setup(healthSystem);
        _warriors[2] = w3;

        // Spawn Zombie
        Vector3 zPos = new Vector3(8f, 4.3f, 0f);
        zombie = Instantiate(zombiePrefab, zPos, Quaternion.identity);
        zombie.name = "Zombie";
        zombie.transform.localScale = new Vector3(-0.8f, 0.8f, 1f);
        Transform hbz = Instantiate(pfHealthBar, zPos + new Vector3(-1f, 4.5f, 0f), Quaternion.identity);
        hbz.GetComponent<HealthBar>().Setup(healthSystem);*/

    }

    // Update is called once per frame
    void Update()
    {
        // TODO: MEJORAR ESTO YA QUE ES BASTANTE INEFICIENTE
        // TODO. ANALIZAR COMO HACER ESTO !!!
        /*bool canPickedWarrior = true;
        foreach (GameObject warrior in _warriors)
        {
            WarriorSM sm = warrior.GetComponent<WarriorSM>();

            if (sm.GetCurrentState().name != "Idle") {
                canPickedWarrior = false;
                break;
            }
        }  

        foreach (GameObject warrior in _warriors) {
            warrior.GetComponent<CapsuleCollider2D>().isTrigger = canPickedWarrior;
        }*/

    }

    void OnGUI() {


    }

}
