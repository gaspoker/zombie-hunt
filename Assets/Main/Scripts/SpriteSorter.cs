using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;

// Source: https://www.youtube.com/watch?v=NeURUTa8iBw
[InitializeOnLoad]
public class SpriteSorter {
    static SpriteSorter() {
        Initialize();
    }

    [RuntimeInitializeOnLoadMethod]
    private static void Initialize() {
        GraphicsSettings.transparencySortMode = TransparencySortMode.CustomAxis;
        GraphicsSettings.transparencySortAxis = new Vector3(0.0f, 1.0f, 1.0f);
    }
}
