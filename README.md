![Zombie Hunt Logo](banner.png)

# Zombie Hunt

Zombie Hunt is a beat 'em up video game developed in Unity 3D 2020 and C#.

## Installation
#### Clone repo (and specify folder name):
```
git clone https://gitlab.com/gaspoker/zombie-hunt.git PROJECT_NAME
```

#### Navigate in folder:
```
cd PROJECT_NAME
```

## Licence

Zombie Hunt is under MIT License. Copyright (c) 2021-2022 by Gaspo Soft (gaspoker@gmail.com).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

![Gaspo Soft Logo](gaspo-soft.png)